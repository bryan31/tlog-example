package com.example.customlabel;

import com.example.customlabel.vo.Person;
import com.yomahub.tlog.core.convert.AspectLogConvert;

public class TestConverter implements AspectLogConvert {
    @Override
    public String convert(Object[] args) {
        Person person = (Person) args[0];
        return person.getId() + "_" + person.getName() + "_" + person.getAddress();
    }
}
